#!/bin/sh
echo "FCD.eu build started..."

#
# JAVASCRIPT
#
appFiles=""

echo "Preparing vendor js files..."

vendor[0]="jquery-2.1.1.min.js"
vendor[1]="snap.svg-min.js"
vendor[2]="svgicons.js"
vendor[3]="svgicons-config.js"

for index in ${!vendor[*]}
do
    printf "   %s\n" ${vendor[$index]}
    appFiles="$appFiles ../assets/javascript/vendor/${vendor[$index]}"
done

echo "Preparing own javascript files..."

app[0]="Tooltip.js"
app[1]="Navigation.js"
app[2]="Init.js"

for index in ${!app[*]}
do
    printf "   %s\n" ${app[$index]}
    appFiles="$appFiles ../assets/javascript/${app[$index]}"
done

#compressJS="./compressjs.sh $appFiles"
#eval $compressJS

uglifyjs -o ../assets/javascript/dist/fcd.js ../assets/javascript/dist/fcd.js

#
# LESS/CSS
#

#echo "Compiling LESS to minified CSS..."
#lessc -x ../assets/styles/less/main.less > ../assets/styles/dist/fcd.css

echo "FCD.eu build COMPLETE"
