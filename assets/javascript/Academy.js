function Academy() {
    this._dashboard = $("div.academy.academy-dashboard");
    this._dashboardN = $("div.academy.academy-dashboard-n");
    this._dashboardToggleButton = $("button.btn-toggle", this._dashboard);
    this._dashboardToggleButtonN = $("button.btn-toggle", this._dashboardN);
    this._dashboardGraphContainer = $("div.graph-container", this._dashboard);
    this._dashboardGraphElement = $("canvas#dashboard-chart", this._dashboardGraphContainer);
    this._dashboardGraphAxis = $("ul.axis", this._dashboard);
    this._dashboardGraphTooltip = $('div.dashboard-chart-tooltip', this._dashboardGraphContainer);
    this._dashboardGraph = null;
    this._dashboardToggled = false;
    this._dashboardToggledN = false;

    this._actionMenu = $("div.academy-actions");

    this._feed = $("div.academy.academy-feed");
    this._filter = $("div.filter", this._feed);
    this._filterToggleButton = $("button.btn-toggle", this._filter);
    this._filterToggled = false;

    this._posts = $("div.post", this._feed);
    this._postEditButtons = $("button.post-edit-button", this._posts);
    this._postEditNavs = $("ul.post-edit-menu", this._posts);

    this._lightboxButton = $(".btn-show-lightbox");
    this._lightboxContainer = $(".lightbox-container");
    this._lightboxIconClose = $(".link-icon-close");

    this._openFileDialogModalButton = $(".btn-show-file_dialog_modal");

    this._academyTooltip = new AcademyTooltip();
}

Academy.prototype.build = function () {
    if (this._dashboard.length) {

        // this._academyTooltip.addTip("Od Vašeho posledního přihlášení přibyly <br/><strong>3 nové uzavřené případy</strong>", 250, 250);
        // this._academyTooltip.addTip("Druhý tip", 200, 250);
        // this._academyTooltip.addTip("Třetí tip", 500, 500);
        // this._academyTooltip.addTip("Čtvrtý tip", 100, 100);
        // this._academyTooltip.addTip("Pátý tip", 50, 50);
        // this._academyTooltip.show();

        this._dashboardToggleButton.click($.proxy(this._toggleDashboard, this));

        $(window).scroll($.proxy(this._toggleActionMenu, this));
        $(window).on("touchstart touchmove", $.proxy(this._toggleActionMenu, this));

        this._filterToggleButton.click($.proxy(this._toggleFilter, this));

          $("body").on("click", "button.post-edit-button", this._togglePostEditMenu.bind(this));
          $("body").on("click", "ul.post-edit-menu", this._hidePostEditMenu.bind(this));
        // this._postEditButtons.click($.proxy(this._togglePostEditMenu, this));
        // this._postEditNavs.click($.proxy(this._hidePostEditMenu, this));

        $("html").click($.proxy(this._hidePostEditMenu, this));
    }

    if (this._dashboardN.length) {
        this._dashboardToggleButtonN.click($.proxy(this._toggleDashboardN, this));
    }

    if (this._lightboxButton.length) {
        this._lightboxButton.click($.proxy(this._showLightbox, this));
        this._lightboxContainer.click($.proxy(this._hideLightbox, this));
        this._lightboxIconClose.click($.proxy(this._hideLightbox, this));
    }

    if (this._openFileDialogModalButton.length) {
        // comments are reloaded after new comment is added. So delegate click action to body.
        $('body').on("click", ".btn-show-file_dialog_modal", this._openFileDialogModal.bind(this));
        // this._openFileDialogModalButton.click($.proxy(this._openFileDialogModal, this));
    }
};

Academy.prototype._buildDashboardChart = function () {
    var ctx = this._dashboardGraphElement.get(0).getContext("2d");
    var data = {
        labels: ACADEMY_GRAPH_DASHBOARD_DATASET_LABELS,
        datasets: [
            {
                fillColor: "rgba(0,0,0,0)",
                strokeColor: "#ef8316",
                pointColor: "#fff",
                pointStrokeColor: "#ef8316",
                pointHighlightFill: "#fff",
                pointHighlightStroke: "#ef8316",
                data: ACADEMY_GRAPH_DASHBOARD_DATASET_MAIN
            }
        ]
    };

    this._dashboardGraph = new Chart(ctx).Line(data, {
        animateScale: true,
        scaleGridLineColor: "#dcdcdc",
        bezierCurve: false,
        //Boolean - Whether to show horizontal lines (except X axis)
        scaleShowHorizontalLines: false,
        //Boolean - Whether to show vertical lines (except Y axis)
        scaleShowVerticalLines: true,
        datasetStrokeWidth: 5,
        pointDotRadius: 4,
        pointDotStrokeWidth: 3,
        scaleFontFamily: "'Ubuntu', sans-serif",
        scaleFontSize: 12,
        scaleFontStyle: "normal",
        scaleFontColor: "#000",
        responsive: true,
        maintainAspectRatio: false,
        scaleShowLabels: false,
        tooltipTemplate: "<%= label %>",
        customTooltips: $.proxy(this._showCustomTooltip, this),
        onAnimationComplete: $.proxy(this._showCurrentStarsRow, this)
    });
};

Academy.prototype._showCurrentStarsRow = function () {
    var lastValue = Math.floor(ACADEMY_GRAPH_DASHBOARD_DATASET_MAIN[ACADEMY_GRAPH_DASHBOARD_DATASET_STARS.length - 1]);
    this._dashboardGraphAxis.find("li[data-stars=" + lastValue + "]").addClass("active");
};

Academy.prototype._showCustomTooltip = function (tooltip) {
    if (!tooltip) {
        this._dashboardGraphTooltip.css({opacity: 0});
        return;
    }

    this._dashboardGraphTooltip.removeClass('above below');
    this._dashboardGraphTooltip.addClass(tooltip.yAlign);

    var index = ACADEMY_GRAPH_DASHBOARD_DATASET_LABELS.indexOf(tooltip.text);

    this._dashboardGraphTooltip.find(".date").text(ACADEMY_GRAPH_DASHBOARD_DATASET_LABELS[index]);
    this._dashboardGraphTooltip.find(".stars")
        .removeClass("stars-0 stars-1 stars-2 stars-3")
        .addClass("stars-" + ACADEMY_GRAPH_DASHBOARD_DATASET_STARS[index]);

    this._dashboardGraphTooltip.find(".dashboard-chart-tooltip-stats-checks").text(ACADEMY_GRAPH_DASHBOARD_DATASET_ISSUES[index]);
    this._dashboardGraphTooltip.find(".dashboard-chart-tooltip-stats-bulbs").text(ACADEMY_GRAPH_DASHBOARD_DATASET_ADVICES[index]);
    this._dashboardGraphTooltip.find(".dashboard-chart-tooltip-stats-likes").text(ACADEMY_GRAPH_DASHBOARD_DATASET_LIKES[index]);

    var tabletRes = $(window).width() <= 918;
    var leftPos = tooltip.chart.canvas.offsetLeft + tooltip.x;
    if (index == 0 && tabletRes) {
        leftPos += 80;
        this._dashboardGraphTooltip.addClass("left");
    } else {
        this._dashboardGraphTooltip.removeClass("left");
    }

    this._dashboardGraphTooltip.css({
        opacity: 1,
        left: leftPos + 'px',
        top: (tooltip.y - 90) + 'px'
    });
};

Academy.prototype._toggleDashboardN = function () {
    if (this._dashboardN.hasClass("academy-dashboard-open")) {

        this._dashboardToggledN = false;
        this._dashboardN.hide();
        this._dashboardN.removeClass("academy-dashboard-open");
        this._dashboardN.fadeIn();
    } else {

        this._dashboardToggledN = true;
        this._dashboardN.hide();
        this._dashboardN.addClass("academy-dashboard-open");
        this._dashboardN.fadeIn();
    }
};

Academy.prototype._toggleDashboard = function () {
    if (this._dashboard.hasClass("academy-dashboard-open")) {

        this._dashboardToggled = false;
        this._dashboard.hide();
        this._dashboardGraph.destroy();
        this._dashboardGraphAxis.find("li").removeClass("active");
        this._dashboardGraphContainer.css({"opacity": 0});
        this._dashboardGraphAxis.css({"opacity": 0});
        this._dashboard.removeClass("academy-dashboard-open");
        this._dashboard.fadeIn();
    } else {

        this._dashboardToggled = true;
        this._dashboard.hide();
        this._dashboard.addClass("academy-dashboard-open");
        this._dashboard.fadeIn($.proxy(function () {
            this._buildDashboardChart();
            this._dashboardGraphContainer.animate({"opacity": 1});
            this._dashboardGraphAxis.animate({"opacity": 1});
        }, this));
    }

    this._toggleActionMenu();
};

Academy.prototype._toggleActionMenu = function () {
    if ((this._actionMenu.height() < $(window).height() && $(window).scrollTop() >= 240 && !this._dashboardToggled) ||
        (this._actionMenu.height() < $(window).height() && $(window).scrollTop() >= 445 && this._dashboardToggled)) {
        this._actionMenu.addClass("fixed");
    } else {
        this._actionMenu.removeClass("fixed");
    }
};

Academy.prototype._toggleFilter = function () {
    if (this._filter.hasClass("filter-active")) {
        this._filter.removeClass("filter-active");
        this._filterToggleButton.text("Rozbalit filtr");

    } else {
        this._filter.addClass("filter-active");
        this._filterToggleButton.text("Sbalit filtr");
    }
};

Academy.prototype._togglePostEditMenu = function (event) {
    event.stopPropagation();
    var button = $(event.currentTarget);
    if (button.hasClass("active")) {

        button.removeClass("active");
        button.parent().find("ul.post-edit-menu").removeClass("active");
    } else {

        button.addClass("active");
        button.parent().find("ul.post-edit-menu").addClass("active");
    }
};

Academy.prototype._hidePostEditMenu = function (e) {
    $("button.post-edit-button").removeClass("active");
    $("ul.post-edit-menu").removeClass("active");
    // this._postEditButtons.removeClass("active");
    // this._postEditNavs.removeClass("active");
};

Academy.prototype._showLightbox = function (e) {
    e.preventDefault();

    var lightboxId = e.currentTarget.getAttribute("data-lightbox-id");
    var lightbox = $(lightboxId);
    var sliderUl = $(lightboxId + " ul:first");
    var sliderLi = $(lightboxId + " ul:first>li");
    lightbox.fadeIn();
    $(".lbc").hide();
    $(".lbc.default").show();
    var slideCount = sliderLi.length;
    var slideWidth = sliderLi.width();
    var sliderUlWidth = slideCount * slideWidth;
    var initialSlide = parseInt(e.currentTarget.getAttribute("data-initial-slide")) || 0;
    var minLeftMargin = -slideWidth * (slideCount - 1);
    var maxLeftmargin = 0;

    var getCurrentLeftMargin = function () {
        return parseInt(sliderUl.css("margin-left").replace('px', ''));
    };

    sliderUl.css({
        width: sliderUlWidth,
        "margin-left": -initialSlide * slideWidth
    });

    $("a.prev").click(function (e) {
        e.preventDefault();
        var currentLeftMargin = getCurrentLeftMargin();
        var newLeftMargin = currentLeftMargin + slideWidth;
        if (newLeftMargin > maxLeftmargin) {
            newLeftMargin = minLeftMargin;
        }
        sliderUl.animate({
            "margin-left": newLeftMargin + 'px'
        });
        stopVideos();
    });

    $("a.next").click(function (e) {
        e.preventDefault();
        var currentLeftMargin = getCurrentLeftMargin();
        var newLeftMargin = currentLeftMargin - slideWidth;
        if (newLeftMargin < minLeftMargin) {
            newLeftMargin = maxLeftmargin;
        }
        sliderUl.animate({
            "margin-left": newLeftMargin + 'px'
        });
        stopVideos();
    });

    $(".js-active-btn, .lbc-back").click(function (e) {
        e.preventDefault();
        $(".lbc").hide();
        var $el = $(this);
        if ($el.hasClass("lbc-comment")) {
            $(".lbc.comment").show();
        } else if ($el.hasClass("lbc-type")) {
            $(".lbc.type").show();
        } else if ($el.hasClass("lbc-back")) {
            $(".lbc.default").show();
        }
    });
};

Academy.prototype._hideLightbox = function (event) {
    var $t = $(event.target);
    var $el = null;

    if ($t.hasClass("lightbox-container")) {
        $el = $t;
    } else if ($t.hasClass("icon-close")) {
        $el = $t.closest(".lightbox-container");
    }
    if ($el) {
        $el.hide(0);
        $("a.prev").off("click");
        $("a.next").off("click");
        stopVideos();
    }
};

Academy.prototype._openFileDialogModal = function (e) {
    e.preventDefault();
    var $fileDialog = $("#file-dialog");
    $fileDialog.fadeIn();
    var internalId = $(e.target).data("internal-id");
    var $input = $fileDialog.find(".internal-id");
    if(internalId && $input.length) {
      $input.val(internalId)
    }
};
