
function Help()
{
    this._help = $("div.help");

    this._accordeon = $("ul.accordeon");
    this._accordeonQuestions = this._accordeon.find("li.accordeon-question");
}

Help.prototype.build = function()
{
    if(this._help.length)
    {
        this._accordeonQuestions.on("click.show", $.proxy(this._openQuestion, this));
    }
};

Help.prototype._openQuestion = function(event)
{
    event.preventDefault();
    var questionElement = $(event.currentTarget);

    if(questionElement.hasClass("active")) {
        this._hideQuestions();
    } else {
        this._hideQuestions();
        questionElement.find("div.accordeon-container").slideDown();
        questionElement.addClass("active");
    }
};

Help.prototype._hideQuestions = function()
{
    this._accordeonQuestions.find("div.accordeon-container").slideUp();
    this._accordeonQuestions.removeClass("active");
};