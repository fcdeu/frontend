(function () {
  window.initTooltipx = function (buttonSelector, tooltipSelector) {
    var button = document.querySelector(buttonSelector)
    var tooltip = document.querySelector(tooltipSelector)

    var instance = Popper.createPopper(button, tooltip, {
      modifiers: [
        {
          name: "offset",
          options: {
            offset: [0, 8],
          },
        },
      ],
    })

    button.addEventListener("click", function () {
      tooltip.setAttribute("data-show", "")
      instance.update()
    })
  }

  $(".tooltipx .close").on("click", function () {
    $(this).parent()[0].removeAttribute("data-show")
  })
})()
