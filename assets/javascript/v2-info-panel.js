$(".info-panel .close").on("click", function () {
  $(".info-panel").addClass("closed")
  $("body").css({
    paddingTop: "50px"
  })
  $(".navbar").css({
    top: "0px"
  })
  $(".sidebar").css({
    top: "50px"
  })
  $(".overlay").addClass("top-50")
})
