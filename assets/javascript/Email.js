var Email = function () {
  this._container = $('.name-whisperer-container')
  this._newMessageForm = $('.message-form.message-new')
  this._nameInput = $('.js-name_whisperer')
  this._whisperer = $('.name-whisperer')
  this._whispererItem = $('.name-whisperer li')
  this._closeButton = $('.remove-people')
}

Email.prototype.build = function () {
  if (this._newMessageForm.length) {
    this._nameInput.on('keyup', debounce($.proxy(this._showWhisperer, this), 400))
    $('body').on('click', this._whispererItem, $.proxy(this._add, this))
    this._container.on('click', this._closeButton, $.proxy(this._closePeople, this))
  }
}

Email.prototype._showWhisperer = function () {
  var self = this
  var value = this._nameInput.val()
  if (value) {
    $.ajax({
      type: 'POST',
      url: '#URL_PRO_NACITANI_UZIVATELU#',
      accept: 'application/json',
      data: {
        filter: value
      },
      success: function (data) {
        self._whisperer.empty()
        if (data.length)
          for (var i = 0; i < data.length; i++)
            $('<li/>')
              .html('<img src="' + data[i].avatar + '">' + data[i].name)
              .attr('data-id', data[i].id)
              .attr('data-name', data[i].name)
              .appendTo(self._whisperer)
      }
    })
  } else {
    this._whisperer.empty()
  }
}

Email.prototype._add = function (e) {
  var target = $(e.target)
  var id = target.attr('data-id')
  var name = target.attr('data-name')
  if (id && name) {
    var span = $('<span/>')
      .html(name + '<a href="#" class="remove-people">&times;</a>')
      .prependTo(this._container)
    $('<input>').attr({
        type: 'hidden',
        name: 'id-' + id
      })
      .prependTo(span)
    this._nameInput.val('')
    this._whisperer.empty()
  }
}

Email.prototype._closePeople = function (e) {
  e.preventDefault()
  var target = $(e.target)
  if (target.hasClass('remove-people'))
    target.parent().remove()
}
