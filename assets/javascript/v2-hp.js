(function () {
  var $win = $(window)
  var $el = $(".js-hp_menu")
  // var $body = $("html, body")

  var onLoad = function () {
    var url = new URL(window.location)
    var section = url.searchParams.get("section") || "autoservisy"
    var tab = url.searchParams.get("tab") || "logger"

    openSection(section, true)
    openTab(tab)
  }

  var updateUrl = function (key, val) {
    var url = new URL(window.location)
    url.searchParams.set(key, val)
    window.history.pushState(null, "", url.toString())
  }

  var openSection = function (section, onLoad) {
    $(".js-tab_btn").removeClass("active")
    $("[data-section=" + section + "]").addClass("active")
    $("[data-section-target]").removeClass("active")

    var $tabContents = $("[data-section-target=" + section + "]")

    $tabContents.addClass("active")
    $tabContents.find(".js-tab_btn").removeClass("active")
    if (!onLoad) {
      $tabContents.find(".js-tab_btn:first-child").click()
    }
  }

  var openTab = function (tab) {
    $("[data-subtab]").removeClass("active")
    $("[data-subtab=" + tab + "]").addClass("active")
    $("[data-subtab-target]").removeClass("active")
    $("[data-subtab-target=" + tab + "]").addClass("active")
  }

  var stopVideos = function () {
    var $videos = $("iframe.video")

    $videos.each(function (_, video) {
      var $video = $(video)
      var src = $video.attr("src")
      $video.attr("src", "")
      $video.attr("src", src)
    })
  }

  var click = function (e) {
    stopVideos()

    var $target = $(e.currentTarget)
    var section = $target.data("section")
    var tab = $target.data("subtab")

    if (section) {
      openSection(section, false)
      updateUrl("section", section)
    }

    if (tab) {
      openTab(tab)
      updateUrl("tab", tab)
    }

    // $body.stop().animate({ scrollTop: 0 }, 500, "swing");
  }

  var toggleMenu = function () {
    $el.toggleClass("d-none", $win.scrollTop() < 175)
  }

  window.addEventListener("scroll", toggleMenu)
  $(".js-tab_btn").on("click", click)

  toggleMenu()
  onLoad()
})()
