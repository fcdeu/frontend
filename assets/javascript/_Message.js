function _Message() {
  this._container = $('.js-msg_container');
}

_Message.prototype.show = function (text) {
  this._container.text(text);
  this._container.removeClass('hidden');
  setTimeout(function () {
    this._hide();
  }.bind(this), 3000);
};

_Message.prototype._hide = function () {
  this._container.addClass('hidden')
};
