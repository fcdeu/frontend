(function () {
  var closeModal = function () {
    $(".modal").addClass("d-none")
  }

  $(".js-open_modal").on("click", function (e) {
    e.preventDefault()
    $($(this).data("target")).removeClass("d-none")
  })

  $(".js-modal_close").on("click", closeModal)

  // $(".modal").on("click", function (e) {
  //   if ($(e.target).hasClass("modal")) {
  //     closeModal()
  //   }
  // })
})()
