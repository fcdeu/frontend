function _File() {
    this._openFileDialogButton = $(".btn-show-file_dialog");
    this._closeFileDialogButton = $(".btn-close");
    this._fileTypeSelect = $("#file-dialog_file-type");

    this._imageUploader = $(".img-upload");
}

_File.prototype.build = function () {
    if (this._openFileDialogButton.length) {
        this._openFileDialogButton.click($.proxy(this._openFileDialog, this));
        this._closeFileDialogButton.click($.proxy(this._closeFileDialog, this));
        this._fileTypeSelect.change($.proxy(this._fileTypeChanged, this));
    }
    if(this._imageUploader.length) {
        this._imageUploader.change($.proxy(this._imageUploaderChange, this));
    }
};

_File.prototype._openFileDialog = function (event) {
    var $input = $("#file-dialog_input");
    var $fileinfo = $(".file-dialog_name-info");
    $input.change(function () {
        $fileinfo.text($input.val().replace(/.*(\/|\\)/, ''));
    });
    $input.click();
};

_File.prototype._closeFileDialog = function (event) {
    event.preventDefault();
    $("#file-dialog").hide(0);
};

_File.prototype._fileTypeChanged = function (event) {
    var option = $("#file-dialog_file-type option:selected");
    var desc = option.data("desc");
    var hideDoc = option.data("doc-hidden");

    $("#file-dialog_file-desc").val(desc ? desc : "");

    $("#doc").toggle(!hideDoc);
};

_File.prototype._imageUploaderChange = function (e) {
  var $fileInput = $(e.currentTarget)
  var path = $fileInput.val()
  path = path.replace(/^.*[\\\/]/, '') // get only filename
  $fileInput.siblings("label").text(path)
}
