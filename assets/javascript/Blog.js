function Blog() {
    this._shareLink = $(".js-share");
}

Blog.prototype.build = function () {
    if (this._shareLink.length) {
        this._shareLink.hover(function (e) {
            var $el = $(e.currentTarget);
            var $popup = $($el.find(".share-popup"));
            $popup.fadeIn();
        }, function () {
            $(".share-popup").hide();
        });
    }
};
