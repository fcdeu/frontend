

/**
 * "DobreServisy" js class
 * @constructor
 */
function DobreServisy()
{
    this._register = $("div.goodservice-register");

    // Form properties
    this._form = $("form", this._register);
    this._formInputAddress = $("input.input-service-address", this._form);
    this._formInputLng = $("input.input-service-lng", this._form);
    this._formInputLat = $("input.input-service-lat", this._form);
    this._formInputPostalCode = $("input.input-service-postal-code", this._form);
    this._formInputStreetNumber = $("input.input-service-street-number", this._form);
    this._formInputCountry = $("input.input-service-country", this._form);
    this._formInputRoute = $("input.input-service-route", this._form);
    this._formInputCity = $("input.input-service-city", this._form);

    this._formPriceInputs = $("input.input-price", this._form);

    // Form "other services" control
    this._formOtherServices = $("fieldset#other-services", this._form);
    this._formOtherServicesList = $("ul.checks", this._formOtherServices);
    this._formOtherServicesAddInput = $("div.add-control input[type=text]", this._formOtherServices);
    this._formOtherServicesAddButton = $("div.add-control button", this._formOtherServices);
    this._formOtherServicesArray = [];

    // "Repair brands" control
    this._formRepairBrands = $("fieldset#repair-brands", this._form);
    this._formRepairBrandsRadioList = $("input[type=radio]", this._formRepairBrands);
    this._formRepairBrandsAddControl = $("div.add-control", this._formRepairBrands);
    this._formRepairBrandsAddInput = $("input[type=text]", this._formRepairBrandsAddControl);
    this._formRepairBrandsAddButton = $("button", this._formRepairBrandsAddControl);
    this._formRepairBrandsAddTagsList = $("ul.tags", this._formRepairBrandsAddControl);
    this._formRepairBrandsAddDropdown = $("ul.dropdown", this._formRepairBrandsAddControl);
    this._formRepairBrandsArray = [];

    // "Opening hours" control
    this._formOpeningHours = $("fieldset#opening-hours", this._form);
    this._formOpeningHoursClosedButtons = $("input[type=checkbox]", this._formOpeningHours);
    this._formOpeningHoursInputs = $("input[type=text]", this._formOpeningHours);

    // Photos control
    this._formPhotos = $("fieldset#photos", this._form);
    this._formPhotosMain = $("ul.photos li.main", this._formPhotos);
    this._formPhotosList = $("ul.photos li.photo-wrapper", this._formPhotos);
    this._formPhotosAdd = $("ul.photos li button.add", this._formPhotos);
    this._formPhotosSortableSettings = {
        items: "> li.photo-wrapper",
        containment: this._formPhotos
    };

    // "Special repair" control
    this._formSpecialRepair = $("fieldset#special-repair", this._form);
    this._formSpecialRepairRadioList = $("input[type=radio]", this._formSpecialRepair);
    this._formSpecialRepairTagsFromRB = $("ul.tags-from-repair-brands", this._formSpecialRepair);
    this._formSpecialRepairAddControl = $("div.add-control", this._formSpecialRepair);
    this._formSpecialRepairAddInput = $("input[type=text]", this._formSpecialRepairAddControl);
    this._formSpecialRepairAddButton = $("button", this._formSpecialRepairAddControl);
    this._formSpecialRepairAddTagsList = $("ul.tags", this._formSpecialRepairAddControl);
    this._formSpecialRepairArray = [];

    // "Authorized" control
    this._formAuthorized = $("fieldset#authorized", this._form);
    this._formAuthorizedRadioList = $("input[type=radio]", this._formAuthorized);
    this._formAuthorizedTagsFromRB = $("ul.tags-from-repair-brands", this._formAuthorized);
    this._formAuthorizedAddControl = $("div.add-control", this._formAuthorized);
    this._formAuthorizedAddInput = $("input[type=text]", this._formAuthorizedAddControl);
    this._formAuthorizedAddButton = $("button", this._formAuthorizedAddControl);
    this._formAuthorizedAddTagsList = $("ul.tags", this._formAuthorizedAddControl);
    this._formAuthorizedArray = [];

    // Map properties
    this._defaultCenterPos = new google.maps.LatLng(50.08804289389093, 14.437859058380127);
    this._map = $("div.map", this._register);
    this._mapStyle = [{"featureType":"water","stylers":[{"color":"#46bcec"},{"visibility":"on"}]},{"featureType":"landscape","stylers":[{"color":"#f2f2f2"}]},{"featureType":"road","stylers":[{"saturation":-100},{"lightness":45}]},{"featureType":"road.highway","stylers":[{"visibility":"simplified"}]},{"featureType":"road.arterial","elementType":"labels.icon","stylers":[{"visibility":"off"}]},{"featureType":"administrative","elementType":"labels.text.fill","stylers":[{"color":"#444444"}]},{"featureType":"transit","stylers":[{"visibility":"off"}]},{"featureType":"poi","stylers":[{"visibility":"off"}]}];
    this._googleMap = null;
    this._geocoder = new google.maps.Geocoder();
    this._geocoderMarker = null;
}

/**
 * Main build of "DobreServisy" registration form
 */
DobreServisy.prototype.build = function()
{
    if(this._register.length) {

        // Map build
        this.buildGoogleMap();
        this.createAddPin();

        // Form build
        this.createRegisterForm();
    }
};

/**
 * Build google map canvas
 */
DobreServisy.prototype.buildGoogleMap = function()
{
    this._googleMapOptions = {
        center: this._defaultCenterPos,
        zoom: 12,
        disableDefaultUI: true,
        panControl: false,
        styles: this._mapStyle,
        panControlOptions: {
            position: google.maps.ControlPosition.TOP_LEFT
        },
        zoomControl: true,
        zoomControlOptions: {
            style: google.maps.ZoomControlStyle.LARGE,
            position: google.maps.ControlPosition.LEFT_TOP
        },

        scrollwheel: false,
        draggable: true,
        mapTypeId: google.maps.MapTypeId.ROADMAP
    };

    this._googleMap = new google.maps.Map(this._map[0], this._googleMapOptions);
    this._map.css("opacity", 1);
};

/**
 * Create draggable google map marker for address picker
 */
DobreServisy.prototype.createAddPin = function()
{
    var image = new google.maps.MarkerImage(BASEPATH + "/assets/images/icon-pin@2x.png", null, null, null, new google.maps.Size(30,61));
    this._geocoderMarker = new google.maps.Marker({
        map: this._googleMap,
        position: this._defaultCenterPos,
        icon: image,
        animation: google.maps.Animation.DROP,
        draggable:true
    });
    this._googleMap.setCenter(this._defaultCenterPos);
    this._googleMap.panBy(0, this._map.height()/3);

    google.maps.event.addListener(this._geocoderMarker, 'dragend', $.proxy(function(event) {

        this._geocoder.geocode({'latLng': event.latLng}, $.proxy(function(results, status) {
            if (status == google.maps.GeocoderStatus.OK) {
                if (results[0]) {
                    this._processAddress(results, event);
                }
            }
        }, this));
    }, this));
};

DobreServisy.prototype._processAddress = function (results, event) {
    var route = this.getComponentFromAddress("route", results[0]);
    if (route) {
        this._formInputRoute.val(route);
    } else {
        this._formInputRoute.val(this.getComponentFromAddress("locality", results[0]));
    }
    var streetNumber = this.getComponentFromAddress("street_number", results[0]);
    if (streetNumber) {
        this._formInputStreetNumber.val(streetNumber);
    } else {
        this._formInputStreetNumber.val(this.getComponentFromAddress("premise", results[0]))
    }
    this._formInputCity.val(this.getComponentFromAddress("political", results[0]));
    this._formInputCountry.val(this.getComponentFromAddress("country", results[0]));
    this._formInputPostalCode.val(this.getComponentFromAddress("postal_code", results[0]));
    if (event) {
        this._formInputLat.val(event.latLng.lat());
        this._formInputLng.val(event.latLng.lng());
    }
    this._formInputAddress.val(results[0].formatted_address);
}

DobreServisy.prototype.getComponentFromAddress = function(componentName, googleResult)
{
    for(var index in googleResult.address_components) {
        var component = googleResult.address_components[index];
        for(var typeIndex in component.types) {
            var type = component.types[typeIndex];
            if(type == componentName) {
                return component.long_name;
            }
        }
    }
    return null;
};

/**
 * Build all parts of register form
 */
DobreServisy.prototype.createRegisterForm = function()
{
    // Address control
    this._formInputAddress.on("focusout", $.proxy(this._synchronizeAddressWithMap, this));

    this._formPriceInputs.numeric({ decimal : ",", decimalPlaces : 2, negative : false});

    // "Other services" control
    this._formOtherServicesAddButton.on("click.addOtherService", $.proxy(this.__addOtherService, this));
    this._formOtherServicesAddInput.on("keypress.addOtherService", $.proxy(this.__addOtherService, this));
    this._formOtherServicesList.find("li").each($.proxy(function(index, element) {
        this._formOtherServicesArray.push($(element).find("label").text().toLowerCase());
    }, this));

    // "Repair brands" control
    this._formRepairBrandsRadioList.on("change.toggleAddControl", $.proxy(this.__toggleRepairBrandAddControl, this));
    this._formRepairBrandsAddButton.on("click.addRepairBrand", $.proxy(this.__addRepairBrand, this));
    this._formRepairBrandsAddInput.on("keypress.addRepairBrand", $.proxy(this.__addRepairBrand, this));
    this._formRepairBrandsAddInput.on("keyup.showRepairBrandDropdown", $.proxy(this.__toggleRepairBrandDropdown, this));
    this._formRepairBrandsAddInput.on("keydown.moveToDropdown", $.proxy(this.__moveToDropdown, this));
    for(var index in REPAIR_BRANDS_ARRAY) {
        var brand = REPAIR_BRANDS_ARRAY[index];
        this._formRepairBrandsArray.push(brand.name.toLowerCase());
    }

    // "Opening hours" control
    this._formOpeningHoursClosedButtons.on("change", $.proxy(this.__toggleOpeningHoursRow, this));
    this._formOpeningHoursInputs.numeric({ decimal : ":", decimalPlaces : 2, negative : false});

    // Photos control
    this._formPhotosMain.find("img").on("click.showFilePicker", $.proxy(this.__showFilePicker, this));
    this._formPhotosMain.find("input[type=file]").on("change.syncFileWithCover", $.proxy(this.__syncFileWithCover, this));
    this._formPhotosAdd.on("click.addPhoto", $.proxy(this.__addPhoto, this));
    this._formPhotosList.each($.proxy(function(index, element) {
        element = $(element);
        element.find("input[type=file]").on("change.syncFileWithCover", $.proxy(this.__syncFileWithCover, this));
        element.find("img").on("click.showFilePicker", $.proxy(this.__showFilePicker, this));
        element.find("button.cross").on("click.removePhoto", $.proxy(this.__removePhoto, this));
    }, this));
    this._formPhotosList.parent().sortable(this._formPhotosSortableSettings);

    // "Special repair" control
    this._formSpecialRepairRadioList.on("change.toggleAddControl", $.proxy(this.__toggleSpecialRepairAddControl, this));
    this._formSpecialRepairAddButton.on("click.addSpecialRepair", $.proxy(this.__addSpecialRepair, this));
    this._formSpecialRepairAddInput.on("keypress.addSpecialRepair", $.proxy(this.__addSpecialRepair, this));
    this._formSpecialRepairAddTagsList.find("li").each($.proxy(function(index, element) {
        this._formSpecialRepairArray.push($(element).ignore("button").text().toLowerCase());
    }, this));

    // "Authorized" control
    this._formAuthorizedRadioList.on("change.toggleAddControl", $.proxy(this.__toggleAuthorizedAddControl, this));
    this._formAuthorizedAddButton.on("click.addSpecialRepair", $.proxy(this.__addAuthorized, this));
    this._formAuthorizedAddInput.on("keypress.addSpecialRepair", $.proxy(this.__addAuthorized, this));
    this._formAuthorizedAddTagsList.find("li").each($.proxy(function(index, element) {
        this._formAuthorizedArray.push($(element).ignore("button").text().toLowerCase());
    }, this));
};

/**
 * Synchronize value of address form input with google map marker
 * - rewrites lat, lng form inputs
 * @private
 */
DobreServisy.prototype._synchronizeAddressWithMap = function()
{
    if(this._formInputAddress.val() && this._formInputAddress.val() != "") {

        this._geocoder.geocode( { 'address': this._formInputAddress.val()}, $.proxy(function(results, status) {
            if (status == google.maps.GeocoderStatus.OK) {
                if(this._geocoderMarker) {

                    this._geocoderMarker.setPosition(results[0].geometry.location);
                    this._googleMap.setCenter(results[0].geometry.location);
                    this._googleMap.panBy(0, this._map.height()/3);

                    this._formInputLat.val(results[0].geometry.location.lat());
                    this._formInputLng.val(results[0].geometry.location.lng());

                    this._processAddress(results)
                }
            } else {
                console.log('Geocode was not successful for the following reason: ' + status);
            }
        }, this));
    }
};

/**
 * Adds custom "other service" to check-list
 * @param event
 * @private
 */
DobreServisy.prototype.__addOtherService = function(event)
{
    if(event.type == "keypress" && event.keyCode != 13) return;
    event.preventDefault();

    // Empty input validate
    if(!this._formOtherServicesAddInput.val() || this._formOtherServicesAddInput.val() == "") {
        this._formOtherServicesAddInput.addClass("error");
        return;
    }

    // Unique service validate
    if($.inArray(this._formOtherServicesAddInput.val().toLowerCase(), this._formOtherServicesArray) >= 0) {
        this._formOtherServicesAddInput.addClass("error");
        alert("Tato služba je již v seznamu. Vyberte ji prosím z seznamu výše.");
        return;
    }

    var timestamp = new Date().getTime();
    var html = "<li><input type=\"checkbox\" name=\"" + OTHER_SERVICES_INPUT_NAME + "_time_" + timestamp + "\" id=\"other_services-time-" + timestamp + "\" value=\"0\" checked>";
    html += "<label for=\"other_services-time-" + timestamp + "\"><span></span>" + this._formOtherServicesAddInput.val() + "</label></li>";
    this._formOtherServicesList.append(html);

    this._formOtherServicesArray.push(this._formOtherServicesAddInput.val());
    this._formOtherServicesAddInput.val("");
    this._formOtherServicesAddInput.removeClass("error");
};

/**
 * Show/Hide "repair brand" control add form
 * @private
 */
DobreServisy.prototype.__toggleRepairBrandAddControl = function()
{
    var checkedRadio = this._formRepairBrands.find("input[type=radio]:checked");
    if(checkedRadio.val() == 0) {
        this._formRepairBrandsAddControl.show();
    } else {
        this._formRepairBrandsAddControl.hide();
    }
};

/**
 * Adds "repair brand" as tag from defined list
 * @private
 */
DobreServisy.prototype.__addRepairBrand = function(event)
{
    if(event.type == "keypress" && event.keyCode != 13) return;
    event.preventDefault();

    // Empty input validate
    if(!this._formRepairBrandsAddInput.val() || this._formRepairBrandsAddInput.val() == "") {
        this._formRepairBrandsAddInput.addClass("error");
        return;
    }

    // Non-unique brand validate
    if($.inArray(this._formRepairBrandsAddInput.val().toLowerCase(), this._formRepairBrandsArray) == -1) {
        this._formRepairBrandsAddInput.addClass("error");
        return;
    }

    var index = $.inArray(this._formRepairBrandsAddInput.val().toLowerCase(), this._formRepairBrandsArray);
    var html = "<li>" + REPAIR_BRANDS_ARRAY[index].name +
        "<input type=\"checkbox\" name=\"rb-" + REPAIR_BRANDS_ARRAY[index].name + "\" checked style=\"display: none;\"> <button type=\"button\">&times;</button></li>";
    this._formRepairBrandsAddTagsList.append(html);

    this._formSpecialRepairTagsFromRB.html(this._formRepairBrandsAddTagsList.html());
    this._formSpecialRepairTagsFromRB.find("button").on("click.removeTag", $.proxy(function(event) {
        $(event.currentTarget).parent().remove();
    }, this));

    this._formAuthorizedTagsFromRB.html(this._formRepairBrandsAddTagsList.html());
    this._formAuthorizedTagsFromRB.find("button").on("click.removeTag", $.proxy(function(event) {
        $(event.currentTarget).parent().remove();
    }, this));

    this._formRepairBrandsAddTagsList.find("li:last button").on("click.removeRepairBrand", $.proxy(this.__removeRepairBrand, this));

    this._formRepairBrandsAddInput.val("");
    this._formRepairBrandsAddInput.removeClass("error");
    this._formRepairBrandsArray[index] = null;
};

/**
 * Removes "repair brand" as tag
 * @param event
 * @private
 */
DobreServisy.prototype.__removeRepairBrand = function(event)
{
    event.preventDefault();
    var tag = $(event.currentTarget).parent();

    var tempArray = REPAIR_BRANDS_ARRAY.slice(0);
    for(var index in tempArray) {
        tempArray[index] = tempArray[index].name.toLowerCase();
    }

    var tagText = tag.ignore("button").text().toLowerCase();
    tagText = tagText.substring(0, tagText.length-1);
    var index = $.inArray(tagText, tempArray);

    this._formRepairBrandsArray[index] = REPAIR_BRANDS_ARRAY[index].name.toLowerCase();
    tag.remove();

    this._formSpecialRepairTagsFromRB.html(this._formRepairBrandsAddTagsList.html());
    this._formSpecialRepairTagsFromRB.find("button").on("click.removeTag", $.proxy(function(event) {
        $(event.currentTarget).parent().remove();
    }, this));

    this._formAuthorizedTagsFromRB.html(this._formRepairBrandsAddTagsList.html());
    this._formAuthorizedTagsFromRB.find("button").on("click.removeTag", $.proxy(function(event) {
        $(event.currentTarget).parent().remove();
    }, this));
};

/**
 * Show/Hide "repair brand" add control dropdown
 * @private
 */
DobreServisy.prototype.__toggleRepairBrandDropdown = function()
{
    this._formRepairBrandsAddDropdown.find("li").remove();

    if(!this._formRepairBrandsAddInput.val() || this._formRepairBrandsAddInput.val() == "") {
        this._formRepairBrandsAddDropdown.hide();
        return;
    }

    var searchTerm = this._formRepairBrandsAddInput.val().toLowerCase();

    searchTerm = searchTerm.replace("š", "s"); // Škoda

    var searchResults = [];
    for(var i in REPAIR_BRANDS_ARRAY) {
        var brand = REPAIR_BRANDS_ARRAY[i];

        if(brand.name.toLocaleLowerCase().indexOf(searchTerm) != -1) {
            searchResults.push({
                name: brand.name,
                index: brand.name.toLocaleLowerCase().indexOf(searchTerm)
            });
        }
    }

    for(var i in searchResults.slice(0, 5)) {
        var result = searchResults[i];

        var before = result.name.substr(0, result.index);
        var strong = result.name.substr(result.index, searchTerm.length);
        var after = result.name.substr(searchTerm.length + result.index);

        var html = "<li tabindex=\"" + i + "\">" + before + "<strong>" + strong + "</strong>" + after + "</li>";
        this._formRepairBrandsAddDropdown.append(html);
    }

    this._formRepairBrandsAddDropdown.find("li")
        .on("click", $.proxy(this.__chooseRepairBrandFromDropdown, this))
        .on("keydown.moveInRepairBrandDropdown", $.proxy(this.__moveInRepairBrandDropdown, this))
        .hover(function(event) {$(event.currentTarget).focus();}, function(event) {$(event.currentTarget).blur();});

    this._formRepairBrandsAddDropdown.show();
};

/**
 * Fill "repair brand" add input by selected brand from dropdown
 * @param event
 * @private
 */
DobreServisy.prototype.__chooseRepairBrandFromDropdown = function(event)
{
    event.preventDefault();
    var item = $(event.currentTarget);
    var text = item.text();

    this._formRepairBrandsAddInput.val(text);
    this._formRepairBrandsAddInput.focus();

    this._formRepairBrandsAddDropdown.hide();
    this._formRepairBrandsAddDropdown.find("li").remove();
};

/**
 * On press "down arrow" on "repair brands" input change focus on first item in dropdown
 * @param event
 * @private
 */
DobreServisy.prototype.__moveToDropdown = function(event)
{
    if(event.keyCode != '40') {
        return;
    }
    event.preventDefault();
    this._formRepairBrandsAddDropdown.find("li:first").focus();
};

/**
 * Support for using arrows in "repair brand" dropdown
 * + on enter adds chosen brand to tag list
 * @param event
 * @private
 */
DobreServisy.prototype.__moveInRepairBrandDropdown = function(event)
{
    if(event.keyCode == '13') {
        this.__chooseRepairBrandFromDropdown(event);
        this.__addRepairBrand(event);
        return;
    }

    if(event.keyCode != '40' && event.keyCode != '38') {
        return;
    }

    event.preventDefault();
    var item = $(event.currentTarget);

    if(event.keyCode == '40') {
        if((item.index() + 1) < this._formRepairBrandsAddDropdown.find("li").length) {
            this._formRepairBrandsAddDropdown.find("li:eq(" + (item.index()+1) +")").focus();
        }

    } else {
        if(item.index() == 0) {
            this._formRepairBrandsAddInput.focus();
        } else {
            this._formRepairBrandsAddDropdown.find("li:eq(" + (item.index()-1) +")").focus();
        }
    }
};

/**
 * Disable/Enable from - to inputs of opening hours row
 * @param event
 * @private
 */
DobreServisy.prototype.__toggleOpeningHoursRow = function(event)
{
    var checkbox = $(event.currentTarget);
    checkbox.parent().parent().find("input[type=text]").attr("disabled", checkbox.is(":checked"));
};

/**
 * Opens file dialog using click action on file input
 * @param event
 * @private
 */
DobreServisy.prototype.__showFilePicker = function(event)
{
    event.preventDefault();
    var cover = $(event.currentTarget);
    cover.parent().parent().find("input[type=file]").click();
};

/**
 * Checks chosen image
 * @param event
 * @private
 */
DobreServisy.prototype.__syncFileWithCover = function (event) {
    var fileInput = $(event.currentTarget);
    this.__readURL(fileInput[0], fileInput.parent().find("img"), fileInput.parent().find("input[type=hidden]").first());
};

/**
 * Adds image thumbnail from file input to img cover element
 * @param input
 * @param coverElement
 * @private
 */
DobreServisy.prototype.__readURL = function (input, coverElement, hiddenElement) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();
        reader.onload = function (e) {
            var img = new Image;
            img.onload = function () {
                var canvas = document.createElement('canvas');
                canvas.width = 84;
                canvas.height = 84;

                var hRatio = canvas.width / img.width;
                var vRatio = canvas.height / img.height;
                var ratio = Math.max(hRatio, vRatio);
                var centerShift_x = (canvas.width - img.width * ratio) / 2;
                var centerShift_y = (canvas.height - img.height * ratio) / 2;

                var ctx = canvas.getContext("2d");
                ctx.clearRect(0, 0, canvas.width, canvas.height);
                ctx.drawImage(img, 0, 0, img.width, img.height, centerShift_x, centerShift_y, img.width * ratio, img.height * ratio);

                coverElement.attr('src', canvas.toDataURL());
                hiddenElement.attr('value', e.target.result);
            }
            img.src = e.target.result;
        };
        reader.readAsDataURL(input.files[0]);
    }
};
/**
 * Adds another photo picker
 * @param event
 * @private
 */
DobreServisy.prototype.__addPhoto = function(event)
{
    event.preventDefault();

    var html = "<li class=\"photo-wrapper\"><button class=\"cross\" type=\"button\">&times;</button>";
    html += "<input type=\"file\" class=\"photo\"><div><img src=\"" + BASEPATH + "/assets/images/icon-ds-photo@2x.png\" alt=\"\"></div>";
    html += "<small>Fotografie<br> servisu</small></li>";
    this._formPhotosAdd.parent().before(html);

    this._formPhotosList.parent().sortable("destroy");
    this._formPhotosList = $("ul.photos li.photo-wrapper", this._formPhotos);
    this._formPhotosList.each($.proxy(function(index, element) {
        element = $(element);

        var upload = element.find("input[type=file]");
        var cover = element.find("img");
        var cross = element.find("button.cross");

        upload.off("change.syncFileWithCover").on("change.syncFileWithCover", $.proxy(this.__syncFileWithCover, this));
        cover.off("click.showFilePicker").on("click.showFilePicker", $.proxy(this.__showFilePicker, this));
        cross.off("click.removePhoto").on("click.removePhoto", $.proxy(this.__removePhoto, this));
    }, this));

    $("ul.photos li.photo-wrapper:last img", this._formPhotos).click();
    this._formPhotosList.parent().sortable(this._formPhotosSortableSettings);
};

/**
 * Removes photo picker
 * @param event
 * @private
 */
DobreServisy.prototype.__removePhoto = function(event)
{
    event.preventDefault();
    var crossBtn = $(event.currentTarget);
    crossBtn.parent().remove();
};

/**
 * Show/Hide "special repair" control add form
 * @private
 */
DobreServisy.prototype.__toggleSpecialRepairAddControl = function()
{
    var checkedRadio = this._formSpecialRepair.find("input[type=radio]:checked");
    if(checkedRadio.val() == 0) {
        this._formSpecialRepairTagsFromRB.show();
        this._formSpecialRepairAddControl.show();
    } else {
        this._formSpecialRepairAddControl.hide();
        this._formSpecialRepairTagsFromRB.hide();
    }
};

/**
 * Adds special repair tag
 * @param event
 * @private
 */
DobreServisy.prototype.__addSpecialRepair = function(event)
{
    if(event.type == "keypress" && event.keyCode != 13) return;
    event.preventDefault();

    // Empty input validate
    if(!this._formSpecialRepairAddInput.val() || this._formSpecialRepairAddInput.val() == "") {
        this._formSpecialRepairAddInput.addClass("error");
        return;
    }

    // Unique brand validate
    if($.inArray(this._formSpecialRepairAddInput.val().toLowerCase(), this._formSpecialRepairArray) > -1) {
        this._formSpecialRepairAddInput.addClass("error");
        alert("Tato jiná značka je již zahrnuta.");
        return;
    }

    var html = "<li>" + this._formSpecialRepairAddInput.val() + " <button type=\"button\">×</button></li>";
    this._formSpecialRepairAddTagsList.append(html);

    this._formSpecialRepairAddTagsList.find("li:last button").on("click.removeSpecialRepair", $.proxy(this.__removeSpecialRepair, this));

    this._formSpecialRepairArray.push(this._formSpecialRepairAddInput.val());
    this._formSpecialRepairAddInput.val("");
    this._formSpecialRepairAddInput.removeClass("error");
};

/**
 * Removes special repair tag
 * @param event
 * @private
 */
DobreServisy.prototype.__removeSpecialRepair = function(event)
{
    event.preventDefault();
    var button = $(event.currentTarget);

    var index = $.inArray(button.parent().ignore("button").text().toLowerCase(), this._formSpecialRepairArray);
    this._formSpecialRepairArray.splice(index, 1);

    button.parent().remove();
};

/**
 * Show/Hide "authorized" control add form
 * @private
 */
DobreServisy.prototype.__toggleAuthorizedAddControl = function()
{
    var checkedRadio = this._formAuthorized.find("input[type=radio]:checked");
    if(checkedRadio.val() == 0) {
        this._formAuthorizedTagsFromRB.show();
        this._formAuthorizedAddControl.show();
    } else {
        this._formAuthorizedAddControl.hide();
        this._formAuthorizedTagsFromRB.hide();
    }
};

/**
 * Adds "authorized" tag
 * @param event
 * @private
 */
DobreServisy.prototype.__addAuthorized = function(event)
{
    if(event.type == "keypress" && event.keyCode != 13) return;
    event.preventDefault();

    // Empty input validate
    if(!this._formAuthorizedAddInput.val() || this._formAuthorizedAddInput.val() == "") {
        this._formAuthorizedAddInput.addClass("error");
        return;
    }

    // Unique brand validate
    if($.inArray(this._formAuthorizedAddInput.val().toLowerCase(), this._formAuthorizedArray) > -1) {
        this._formAuthorizedAddInput.addClass("error");
        alert("Tato jiná značka je již zahrnuta.");
        return;
    }

    var html = "<li>" + this._formAuthorizedAddInput.val() + " <button type=\"button\">×</button></li>";
    this._formAuthorizedAddTagsList.append(html);

    this._formAuthorizedAddTagsList.find("li:last button").on("click.removeSpecialRepair", $.proxy(this.__removeAuthorized, this));

    this._formAuthorizedArray.push(this._formAuthorizedAddInput.val());
    this._formAuthorizedAddInput.val("");
    this._formAuthorizedAddInput.removeClass("error");
};

/**
 * Removes "authorized" tag
 * @param event
 * @private
 */
DobreServisy.prototype.__removeAuthorized = function(event)
{
    event.preventDefault();
    var button = $(event.currentTarget);

    var index = $.inArray(button.parent().ignore("button").text().toLowerCase(), this._formAuthorizedArray);
    this._formAuthorizedArray.splice(index, 1);

    button.parent().remove();
};
