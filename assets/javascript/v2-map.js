var initMap = function () {
  var options = {
    center: new google.maps.LatLng(50.08804289389093, 14.437859058380127),
    zoom: 12,
    disableDefaultUI: true,
    panControl: false,
    styles: [{
      featureType: "water",
      stylers: [
        {color: "#46BCEC"},
        {visibility: "on"}
      ]
    }, {
      featureType: "landscape",
      stylers: [
        {color: "#F2F2F2"}
      ]
    }, {
      featureType: "road",
      stylers: [
        {saturation: -100},
        {lightness: 45}
      ]
    }, {
      featureType: "road.highway",
      stylers: [
        {visibility: "simplified"}
      ]
    }, {
      featureType: "road.arterial",
      elementType: "labels.icon",
      stylers: [
        {visibility: "off"}
      ]
    }, {
      featureType: "administrative",
      elementType: "labels.text.fill",
      stylers: [
        {color: "#444444"}
      ]
    }, {
      featureType: "transit",
      stylers: [
        {visibility: "off"}
      ]
    }, {
      featureType: "poi",
      stylers: [
        {visibility: "off"}
      ]
    }],
    panControlOptions: {
      position: google.maps.ControlPosition.TOP_LEFT
    },
    zoomControl: true,
    zoomControlOptions: {
      style: google.maps.ZoomControlStyle.LARGE,
      position: google.maps.ControlPosition.LEFT_TOP
    },
    scrollwheel: false,
    draggable: true,
    mapTypeId: google.maps.MapTypeId.ROADMAP
  }

  var map = new google.maps.Map(document.getElementsByClassName("map")[0], options)

  if (mapClickHandler) {
    map.addListener('click', function (mapsMouseEvent) {
      mapClickHandler("" + mapsMouseEvent.latLng.lat() + "," + mapsMouseEvent.latLng.lng())
    })
  }

  function marker(map, options) {
    this.options = options
    this.options.pos = new google.maps.LatLng(options.lat, options.lng)
    this.setMap(map)
  }

  marker.prototype = new google.maps.OverlayView()

  marker.prototype.draw = function () {
    var self = this
    if (!this.div) {
      this.div = document.createElement("div")
      this.div.className = "map-marker"
      this.div.innerHTML += "<img src='" + this.options.img + "'/>" +
        "<ul class='score p-0'>" +
        "<li class='star" + (this.options.stars >= 1 ? " star_active" : "") + "'></li>" +
        "<li class='star" + (this.options.stars >= 2 ? " star_active" : "") + "'></li>" +
        "<li class='star" + (this.options.stars >= 3 ? " star_active" : "") + "'></li>" +
        "</ul>"

      var panes = this.getPanes()
      panes.overlayImage.appendChild(this.div)
    }

    var pos = this.getProjection().fromLatLngToDivPixel(this.options.pos)
    this.div.style.left = pos.x + "px"
    this.div.style.top = pos.y + "px"

    google.maps.event.addDomListener(this.div, "click", function () {
      window.location.href = self.options.url
    })
  }

  for (var i = 0; i < markers.length; i++) {
    new marker(map, markers[i])
  }
}
