function _Lightbox() {
  this._lightboxContainer = $(".lightbox-container");
  this._lightboxIconClose = $(".link-icon-close");
}

_Lightbox.prototype.build = function () {
  if(this._lightboxContainer.length) {
    this._lightboxContainer.click($.proxy(this._hideLightbox, this));
    this._lightboxIconClose.click($.proxy(this._hideLightbox, this));
  }
};

_Lightbox.prototype._hideLightbox = function (event) {
  var target = $(event.target);

  if (target.hasClass("lightbox-container") || target.hasClass("icon-close")) {
    $(".lightbox-container").hide(0);
    $("a.prev").off("click");
    $("a.next").off("click");
    stopVideos();
  }
};
