function Userbox() {
  this._register = $("div.user-box")

  this._formSystems = $("fieldset#systems", this._register);
  this._formSystemsRadioList = $("input[type=radio]", this._formSystems);
  this._formSystemsAddControl = $("div.add-control", this._formSystems);
  this._formSystemsAddInput = $("input[type=text]", this._formSystemsAddControl);
  this._formSystemsAddButton = $("button", this._formSystemsAddControl);
  this._formSystemsAddTagsList = $("ul.tags", this._formSystemsAddControl);
  this._formSystemsAddDropdown = $("ul.dropdown", this._formSystemsAddControl);
  this._formSystemsArray = [];

  this._formBrands = $("fieldset#brands", this._register);
  this._formBrandsRadioList = $("input[type=radio]", this._formBrands);
  this._formBrandsAddControl = $("div.add-control", this._formBrands);
  this._formBrandsAddInput = $("input[type=text]", this._formBrandsAddControl);
  this._formBrandsAddButton = $("button", this._formBrandsAddControl);
  this._formBrandsAddTagsList = $("ul.tags", this._formBrandsAddControl);
  this._formBrandsAddDropdown = $("ul.dropdown", this._formBrandsAddControl);
  this._formBrandsArray = [];
}

Userbox.prototype.build = function () {
  if (this._register.length) {
    this.buildUserBox();
  }
};

Userbox.prototype.buildUserBox = function () {
  this._formSystemsRadioList.on("change.toggleAddControl", $.proxy(this.__toggleRepairSystemAddControl, this));
  this._formSystemsAddButton.on("click.addRepairBrand", $.proxy(this.__addSystem, this));
  this._formSystemsAddInput.on("keypress.addRepairBrand", $.proxy(this.__addSystem, this));
  this._formSystemsAddInput.on("keyup.showRepairBrandDropdown", $.proxy(this.__toggleSystemsDropdown, this));
  this._formSystemsAddInput.on("keydown.moveToDropdown", $.proxy(this.__systemsMoveToDropdown, this));

  this._formBrandsRadioList.on("change.toggleAddControl", $.proxy(this.__toggleRepairBrandAddControl, this));
  this._formBrandsAddButton.on("click.addRepairBrand", $.proxy(this.__addRepairBrand, this));
  this._formBrandsAddInput.on("keypress.addRepairBrand", $.proxy(this.__addRepairBrand, this));
  this._formBrandsAddInput.on("keyup.showRepairBrandDropdown", $.proxy(this.__toggleRepairBrandDropdown, this));
  this._formBrandsAddInput.on("keydown.moveToDropdown", $.proxy(this.__moveToDropdown, this));

  for (var i in REPAIR_BRANDS_ARRAY) {
    this._formBrandsArray.push(REPAIR_BRANDS_ARRAY[i].name.toLowerCase());
  }

  for (var i in REPAIR_BRANDS_ARRAY_PREDEFINED) {
    this.__addRepairBrand(null, REPAIR_BRANDS_ARRAY_PREDEFINED[i].name.toLowerCase())
  }

  for (var i in SYSTEM_SPECIALIZATION_ARRAY) {
    this._formSystemsArray.push(SYSTEM_SPECIALIZATION_ARRAY[i].name.toLowerCase());
  }

  for (var i in SYSTEM_SPECIALIZATION_ARRAY_PREDEFINED) {
    this.__addSystem(null, SYSTEM_SPECIALIZATION_ARRAY_PREDEFINED[i].name.toLowerCase())
  }
};

/**
 * Show/Hide "repair brand" control add form
 */
Userbox.prototype.__toggleRepairBrandAddControl = function () {
  var checkedRadio = this._formBrands.find("input[type=radio]:checked");
  if (checkedRadio.val() == 0) {
    this._formBrandsAddControl.show();
  } else {
    this._formBrandsAddControl.hide();
  }
};

Userbox.prototype.__toggleRepairSystemAddControl = function () {
  var checkedRadio = this._formSystems.find("input[type=radio]:checked");
  if (checkedRadio.val() == 0) {
    this._formSystemsAddControl.show();
  } else {
    this._formSystemsAddControl.hide();
  }
};

/**
 * Adds "repair brand" as tag from defined list
 */
Userbox.prototype.__addRepairBrand = function (e, brand) {
  if (e) {
    if (e.type == "keypress" && e.keyCode != 13) return;
    e.preventDefault();
  } else {
    this._formBrandsAddInput.val(brand);
  }

  // Empty input validate
  if (!this._formBrandsAddInput.val() || this._formBrandsAddInput.val() == "") {
    this._formBrandsAddInput.addClass("error");
    return;
  }

  // Non-unique brand validate
  if ($.inArray(this._formBrandsAddInput.val().toLowerCase(), this._formBrandsArray) == -1) {
    this._formBrandsAddInput.addClass("error");
    return;
  }

  var index = $.inArray(this._formBrandsAddInput.val().toLowerCase(), this._formBrandsArray);
  var repairBrand = REPAIR_BRANDS_ARRAY[index]
  var html = "<li>" + repairBrand.name +
    "<input type=\"checkbox\" name=\"rb-" + repairBrand.name + "\"" +
    " value=\"" + repairBrand.id + "\"" +
    " checked style=\"display: none;\"> <button type=\"button\">&times;</button></li>";
  this._formBrandsAddTagsList.append(html);

  this._formBrandsAddTagsList.find("li:last button").on("click.removeRepairBrand", $.proxy(this.__removeRepairBrand, this));

  this._formBrandsAddInput.val("");
  this._formBrandsAddInput.removeClass("error");
  this._formBrandsArray[index] = null;
};

Userbox.prototype.__addSystem = function (e, system) {
  if(e) {
    if (e.type == "keypress" && e.keyCode != 13) return;
    e.preventDefault();
  } else {
    this._formSystemsAddInput.val(system);
  }

  // Empty input validate
  if (!this._formSystemsAddInput.val() || this._formSystemsAddInput.val() == "") {
    this._formSystemsAddInput.addClass("error");
    return;
  }

  // Non-unique brand validate
  if ($.inArray(this._formSystemsAddInput.val().toLowerCase(), this._formSystemsArray) == -1) {
    this._formSystemsAddInput.addClass("error");
    return;
  }

  var index = $.inArray(this._formSystemsAddInput.val().toLowerCase(), this._formSystemsArray);
  var specSystem =  SYSTEM_SPECIALIZATION_ARRAY[index]
  var html = "<li>" + specSystem.name +
    "<input type=\"checkbox\" name=\"rb-" + specSystem.name + "\"" +
    " value=\"" + specSystem.id + "\"" +
    " checked style=\"display: none;\"> <button type=\"button\">&times;</button></li>";
  this._formSystemsAddTagsList.append(html);

  this._formSystemsAddTagsList.find("li:last button").on("click.removeRepairBrand", $.proxy(this.__removeSystem, this));

  this._formSystemsAddInput.val("");
  this._formSystemsAddInput.removeClass("error");
  this._formSystemsArray[index] = null;
};


/**
 * Removes "repair brand" as tag
 */
Userbox.prototype.__removeRepairBrand = function (e) {
  e.preventDefault();
  var tag = $(e.currentTarget).parent();

  var tempArray = REPAIR_BRANDS_ARRAY.slice(0);
  for (var index in tempArray) {
    tempArray[index] = tempArray[index].name.toLowerCase();
  }

  var tagText = tag.ignore("button").text().toLowerCase();
  tagText = tagText.substring(0, tagText.length - 1);
  var index = $.inArray(tagText, tempArray);

  this._formBrandsArray[index] = REPAIR_BRANDS_ARRAY[index].name.toLowerCase();
  tag.remove();
};

Userbox.prototype.__removeSystem = function (e) {
  e.preventDefault();
  var tag = $(e.currentTarget).parent();

  var tempArray = SYSTEM_SPECIALIZATION_ARRAY.slice(0);
  for (var index in tempArray) {
    tempArray[index] = tempArray[index].name.toLowerCase();
  }

  var tagText = tag.ignore("button").text().toLowerCase();
  tagText = tagText.substring(0, tagText.length - 1);
  var index = $.inArray(tagText, tempArray);

  this._formSystemsArray[index] = SYSTEM_SPECIALIZATION_ARRAY[index].name.toLowerCase();
  tag.remove();
};


/**
 * Show/Hide "repair brand" add control dropdown
 */
Userbox.prototype.__toggleRepairBrandDropdown = function () {
  this._formBrandsAddDropdown.find("li").remove();

  if (!this._formBrandsAddInput.val() || this._formBrandsAddInput.val() == "") {
    this._formBrandsAddDropdown.hide();
    return;
  }

  var searchTerm = this._formBrandsAddInput.val().toLowerCase();

  searchTerm = searchTerm.replace("š", "s"); // Škoda

  var searchResults = [];
  for (var i in REPAIR_BRANDS_ARRAY) {
    var brand = REPAIR_BRANDS_ARRAY[i];

    if (brand.name.toLocaleLowerCase().indexOf(searchTerm) != -1) {
      searchResults.push({
        name: brand.name,
        index: brand.name.toLocaleLowerCase().indexOf(searchTerm)
      });
    }
  }

  for (var i in searchResults.slice(0, 5)) {
    var result = searchResults[i];

    var before = result.name.substr(0, result.index);
    var strong = result.name.substr(result.index, searchTerm.length);
    var after = result.name.substr(searchTerm.length + result.index);

    var html = "<li tabindex=\"" + i + "\">" + before + "<strong>" + strong + "</strong>" + after + "</li>";
    this._formBrandsAddDropdown.append(html);
  }

  this._formBrandsAddDropdown.find("li")
    .on("click", $.proxy(this.__chooseRepairBrandFromDropdown, this))
    .on("keydown.moveInRepairBrandDropdown", $.proxy(this.__moveInRepairBrandDropdown, this))
    .hover(function (event) {$(event.currentTarget).focus();}, function (event) {$(event.currentTarget).blur();});

  this._formBrandsAddDropdown.show();
};

Userbox.prototype.__toggleSystemsDropdown = function () {
  this._formSystemsAddDropdown.find("li").remove();

  if (!this._formSystemsAddInput.val() || this._formSystemsAddInput.val() == "") {
    this._formSystemsAddDropdown.hide();
    return;
  }

  var searchTerm = this._formSystemsAddInput.val().toLowerCase();

  searchTerm = searchTerm.replace("š", "s"); // Škoda

  var searchResults = [];
  for (var i in SYSTEM_SPECIALIZATION_ARRAY) {
    var brand = SYSTEM_SPECIALIZATION_ARRAY[i];

    if (brand.name.toLocaleLowerCase().indexOf(searchTerm) != -1) {
      searchResults.push({
        name: brand.name,
        index: brand.name.toLocaleLowerCase().indexOf(searchTerm)
      });
    }
  }

  for (var i in searchResults.slice(0, 5)) {
    var result = searchResults[i];

    var before = result.name.substr(0, result.index);
    var strong = result.name.substr(result.index, searchTerm.length);
    var after = result.name.substr(searchTerm.length + result.index);

    var html = "<li tabindex=\"" + i + "\">" + before + "<strong>" + strong + "</strong>" + after + "</li>";
    this._formSystemsAddDropdown.append(html);
  }

  this._formSystemsAddDropdown.find("li")
    .on("click", $.proxy(this.__chooseSystemsFromDropdown, this))
    .on("keydown.moveInRepairBrandDropdown", $.proxy(this.__moveInSystemsDropdown, this))
    .hover(function (event) {$(event.currentTarget).focus();}, function (event) {$(event.currentTarget).blur();});

  this._formSystemsAddDropdown.show();
};

/**
 * Fill "repair brand" add input by selected brand from dropdown
 */
Userbox.prototype.__chooseRepairBrandFromDropdown = function (e) {
  e.preventDefault();
  var item = $(e.currentTarget);
  var text = item.text();

  this._formBrandsAddInput.val(text);
  this._formBrandsAddInput.focus();

  this._formBrandsAddDropdown.hide();
  this._formBrandsAddDropdown.find("li").remove();
};

Userbox.prototype.__chooseSystemsFromDropdown = function (e) {
  e.preventDefault();
  var item = $(e.currentTarget);
  var text = item.text();

  this._formSystemsAddInput.val(text);
  this._formSystemsAddInput.focus();

  this._formSystemsAddDropdown.hide();
  this._formSystemsAddDropdown.find("li").remove();
};

/**
 * On press "down arrow" on "repair brands" input change focus on first item in dropdown
 */
Userbox.prototype.__moveToDropdown = function (e) {
  if (e.keyCode != '40') {
    return;
  }
  e.preventDefault();
  this._formBrandsAddDropdown.find("li:first").focus();
};

Userbox.prototype.__systemsMoveToDropdown = function (e) {
  if (e.keyCode != '40') {
    return;
  }
  e.preventDefault();
  this._formSystemsAddDropdown.find("li:first").focus();
};

/**
 * Support for using arrows in "repair brand" dropdown
 * + on enter adds chosen brand to tag list
 */
Userbox.prototype.__moveInRepairBrandDropdown = function (e) {
  if (e.keyCode == '13') {
    this.__chooseRepairBrandFromDropdown(e);
    this.__addRepairBrand(e);
    return;
  }

  if (e.keyCode != '40' && e.keyCode != '38') {
    return;
  }

  e.preventDefault();
  var item = $(e.currentTarget);

  if (e.keyCode == '40') {
    if ((item.index() + 1) < this._formBrandsAddDropdown.find("li").length) {
      this._formBrandsAddDropdown.find("li:eq(" + (item.index() + 1) + ")").focus();
    }
  } else {
    if (item.index() == 0) {
      this._formBrandsAddInput.focus();
    } else {
      this._formBrandsAddDropdown.find("li:eq(" + (item.index() - 1) + ")").focus();
    }
  }
};

Userbox.prototype.__moveInSystemsDropdown = function (e) {
  if (e.keyCode == '13') {
    this.__chooseSystemsFromDropdown(e);
    this.__addSystem(e);
    return;
  }

  if (e.keyCode != '40' && e.keyCode != '38') {
    return;
  }

  e.preventDefault();
  var item = $(e.currentTarget);

  if (e.keyCode == '40') {
    if ((item.index() + 1) < this._formSystemsAddDropdown.find("li").length) {
      this._formSystemsAddDropdown.find("li:eq(" + (item.index() + 1) + ")").focus();
    }
  } else {
    if (item.index() == 0) {
      this._formSystemsAddInput.focus();
    } else {
      this._formSystemsAddDropdown.find("li:eq(" + (item.index() - 1) + ")").focus();
    }
  }
};
