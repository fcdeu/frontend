(function () {

  $(".js-tab").on("click", function (e) {
    var $target = $(e.target)
    var $slide = $target.closest(".carousel-slide")

    $slide.find(".tab-active").removeClass("tab-active")
    $target.parent().addClass("tab-active")
  })

})()
