function Pricing() {
    this._accessCodeInput = $('#access-code');
    this._accessCodeButton = $('#access-code-button');
}

Pricing.prototype.build = function () {
    if (this._accessCodeInput.length) {
        var that = this;
        this._accessCodeInput.on('keyup', function (e) {
            if (that._accessCodeInput.val().length) {
                that._accessCodeButton.removeClass('disabled');
            } else {
                that._accessCodeButton.addClass('disabled');
            }
        })
    }
};
