(function () {
  var filesCount = 0

  // add file from modal window to document
  $(".js-modal-add_file form").submit(function (e) {
    e.preventDefault()
    filesCount++

    // get values
    var $fileInput = $(this).find(".js-file_content").clone() // we can't move file content only
                                                              // so copy whole element
    var fileRealName = $fileInput.val().split('\\').pop() // remove fake path and get only file name
    var fileName = $(this).find(".js-file_name").val() //
    var fileDescription = $(this).find(".js-file_description").val()
    var fileDocumentation = $(this).find(".js-file_documentation:checked").val()

    // add new file to document
    var $files = $(".js-comment-files")
    var fileTemplate = $("#file-uploaded-template")
    $files.append(fileTemplate.html())

    // update documentation ids and names
    var $file = $files.find(".file-uploaded").last()

    $file.find(".js-file_documentation").each(function (index) {
      $(this).find("input")
        .attr("id", "file-doc-" + filesCount + "" + index)
        .attr("name", "file-doc-" + filesCount)
      $(this).find("label")
        .attr("for", "file-doc-" + filesCount + "" + index)
    })

    // set values
    $file.find(".js-file_content-wrapper").append($fileInput)
    $file.find(".js-file_real-name").text(fileRealName)
    $file.find(".js-file_name").val(fileName)
    $file.find(".js-file_description").val(fileDescription)
    $file.find(".js-file_documentation input[value=" + fileDocumentation + "]").prop("checked", true)

    // reset current form
    $(this).trigger("reset")
  })

  // remove file from document
  $(".js-comment-files").on("click", ".js-remove", function () {
    $(this).closest(".file-uploaded").remove()
  })
})()
