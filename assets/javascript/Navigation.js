function Navigation() {
  this._elBtnMenu = $("span.btn-navigation");
  this._elMobileMenu = $("ul.menu-mobile");

  this._loginButton = $(".js-index_login");
  this._closeButton = $(".link-icon-close");
  this._loginDialog = $("#login-dialog");
  this._lightboxContainer = $(".lightbox-container");
}

Navigation.prototype.build = function () {
  if (this._elBtnMenu.length > 0) {
    this._elBtnMenu.click($.proxy(this._toggleMobileMenu, this));
    new svgIcon(this._elBtnMenu[0], svgIconConfig, {easing: mina.backin, speed: 400});
  }

  this._loginButton.click($.proxy(this._showLoginDialog, this));
  this._closeButton.click($.proxy(this._hideLoginDialog, this));
  this._lightboxContainer.click($.proxy(this._hideLoginDialog, this));
};

Navigation.prototype._toggleMobileMenu = function () {
  this._elMobileMenu.fadeToggle(400);
};

Navigation.prototype._showLoginDialog = function (event) {
  event.preventDefault();
  this._loginDialog.fadeIn();
};

Navigation.prototype._hideLoginDialog = function (event) {
  var target = $(event.target);
  if (target.hasClass("lightbox-container") || target.hasClass("icon-close")) {
    event.preventDefault();
    this._loginDialog.hide(0);
  }
};
