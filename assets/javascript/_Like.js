function _Like() {
  this._popupBtn = $("[data-like-popup]");
  this._likeBtn = $(".btn-thumbs-up");
  this._dislikeBtn = $(".btn-thumbs-down");
  this._actionListLink = $(".js-action_list");
  this._adminLikeLightbox = $("#admin-like-lightbox");
  this._adminLikeListLightbox = $("#admin-like-list-lightbox");

  this.clicks = 0;
  this.clickDelay = 400;
  this.clickTimer = null;
}

_Like.prototype.build = function () {
  if (this._popupBtn.length) {
    this._popupBtn.click($.proxy(this._showLikePopup, this));
  }
  if (this._likeBtn.length) {
    this._likeBtn.click($.proxy(this._showLikeMessage, this));
  }
  if (this._dislikeBtn.length) {
    this._dislikeBtn.click($.proxy(this._showDislikeMessage, this));
  }
  if (this._actionListLink.length) {
    this._actionListLink.click($.proxy(this._toggleActionList, this));
  }
  if (this._adminLikeLightbox.length) {
    this._likeBtn.click($.proxy(this._showAdminLikeLightbox, this));
  }
};

_Like.prototype._showLikePopup = function (e) {
  var $el = $(e.currentTarget);
  var id = $el.data("like-popup");
  var html = $("#like-popup-template").html();
  var $body = $("body");
  $body.append(html);
  $body.addClass("modal-open");
  var $form = $("#like-popup-form");
  $form.attr("action", function (i, val) {
    return val + id;
  });
  $form.css({
    left: $el.offset().left,
    top: $el.offset().top - $(document).scrollTop()
  });
  $form.click(function (e) {
    e.stopPropagation(); // do not hide popup
  });
  $form.find(".icon-close").click($.proxy(this._hideLikePopup, this));
  $(".like-popup-container").click($.proxy(this._hideLikePopup, this));
};

_Like.prototype._hideLikePopup = function () {
  $("body").removeClass("modal-open");
  $(".like-popup-container").remove();
};

_Like.prototype._showLikeMessage = function () {
  fcd._message.show("přidáno do tvých oblíbených příspěvků");
}

_Like.prototype._showDislikeMessage = function () {
  fcd._message.show("odebráno z tvých oblíbených příspěvků");
}

_Like.prototype._toggleActionList = function (e) {
  this.clicks++;
  if (this.clicks === 1) {
    this.clickTimer = setTimeout(function () {
      var $target = $(e.target);
      $target.find(".action-list").toggleClass("hidden");
      this.clicks = 0;
    }.bind(this), this.clickDelay);
  } else {
    clearTimeout(this.clickTimer);
    this._adminLikeListLightbox.show();
    this.clicks = 0;
  }
}

_Like.prototype._showAdminLikeLightbox = function () {
  this._adminLikeLightbox.show();
}
