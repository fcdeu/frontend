(function () {
  var $carousel = $(".carousel-slides")

  $carousel.each((i, el) => {
    var activeSlide = 0
    var $slides = $(el).find(".carousel-slide")

    var handleSlideChange = function () {
      if (activeSlide >= $slides.length) {
        activeSlide = 0
      }

      if (activeSlide < 0) {
        activeSlide = $slides.length - 1
      }

      $(el).find(".carousel-slide-active").removeClass("carousel-slide-active")
      $slides.eq(activeSlide).addClass("carousel-slide-active")
    }

    $("[data-slide-index]").on("click", function (e) {
      activeSlide = parseInt($(e.currentTarget).data("slide-index"), 10)
      handleSlideChange()
    })

    $(".js-slide-next").on("click", function () {
      activeSlide++
      handleSlideChange()
    })
    $(".js-slide-prev").on("click", function () {
      activeSlide--
      handleSlideChange()
    })
  })
})()
