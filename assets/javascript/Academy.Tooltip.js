
function AcademyTooltip()
{
    this._tips = [];
    this._currentIndex = 0;
    this._speed = "fast";
    this._remap();
}

AcademyTooltip.prototype._remap = function()
{
    if(this._nextButton) this._nextButton.off("click.nextTip");
    if(this._closeButton) this._closeButton.off("click.close");
    if(this._pagingEls) this._pagingEls.off("click.changeTip");

    this._tooltip = $("div.academy-tooltip");
    this._message = $("p", this._tooltip);
    this._nextButton = $("button.next", this._tooltip);
    this._closeButton = $("button.btn-dismiss", this._tooltip);
    this._pagingEls = $("ul.paging li", this._tooltip);

    this._nextButton.on("click.nextTip", $.proxy(this.nextTip, this));
    this._closeButton.on("click.close", $.proxy(this.close, this));
    this._pagingEls.on("click.changeTip", $.proxy(this._changeTip, this));
};

AcademyTooltip.prototype.addTip = function(message, posX, posY)
{
    this._tips.push({
        message: message,
        posX: posX,
        posY: posY
    });
};

AcademyTooltip.prototype._generate = function(index)
{
    var tip = this._tips[index];

    var HTML = "<div class=\"academy-tooltip\"><div class=\"box\"><p>";
    HTML += tip.message;
    HTML += "</p><button type=\"button\" class='next'>Další tip</button>";
    HTML += "<div class=\"cl\"></div><ul class=\"paging\">";
    for(var i = 0; i < this._tips.length; i++) {
        if(i == index) {
            HTML += "<li data-index=\"" + i + "\" class=\"active\"></li>";
        } else {
            HTML += "<li data-index=\"" + i + "\" ></li>";
        }
    }
    HTML += "</ul><button type=\"button\" class=\"btn-dismiss\">&nbsp;</button>";
    HTML += "</div></div>";
    return HTML;
};

AcademyTooltip.prototype.show = function()
{
    $("body").append(this._generate(this._currentIndex));

    this._remap();

    var tip = this._tips[this._currentIndex];
    this._tooltip.find(".box").css({
        "left": tip.posX + "px",
        "top": tip.posY + "px"
    });
    this._tooltip.fadeIn(this._speed);
};

AcademyTooltip.prototype.close = function()
{
    this._tooltip.fadeOut(this._speed, $.proxy(function() {
        this._currentIndex = 0;
        this._tooltip.remove();
    }, this));
};

AcademyTooltip.prototype.nextTip = function()
{
    this._currentIndex++;
    this._redraw();
};

AcademyTooltip.prototype._redraw = function()
{
    var tip = this._tips[this._currentIndex];
    this._tooltip.fadeOut(this._speed, $.proxy(function() {

        this._tooltip.find(".box").css({
            "left": tip.posX + "px",
            "top": tip.posY + "px"
        });

        this._tooltip.find("li.active").removeClass("active");
        this._tooltip.find("li[data-index=" + this._currentIndex + "]").addClass("active");
        this._message.html(tip.message);

        if((this._currentIndex+1) == this._tips.length) {
            this._nextButton.hide();
        } else {
            this._nextButton.show();
        }

        this._tooltip.fadeIn(this._speed);

    }, this));
};

AcademyTooltip.prototype._changeTip = function(event)
{
    var li = $(event.currentTarget);
    this._currentIndex = parseInt(li.attr("data-index"));
    this._redraw();
};