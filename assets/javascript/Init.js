function FCD() {
    this._tooltip = new Tooltip();
    this._navigation = new Navigation();
    this._dobreServisy = new DobreServisy();
    this._academy = new Academy();
    this._help = new Help();
    this._blog = new Blog();
    this._file = new _File();
    this._like = new _Like();
    this._lightbox = new _Lightbox();
    this._diagnosticCase = new DiagnosticCase();
    this._pricing = new Pricing();
    this._email = new Email();
    this._userbox = new Userbox();
    this._message = new _Message();
}

FCD.prototype.build = function () {
    this._navigation.build();
    this._tooltip.build();
    this._dobreServisy.build();
    this._academy.build();
    this._help.build();
    this._blog.build();
    this._file.build();
    this._like.build();
    this._lightbox.build();
    this._diagnosticCase.build();
    this._pricing.build();
    this._email.build();
    this._userbox.build();
};

var fcd = null;
$(document).ready(function () {
    fcd = new FCD();
    fcd.build();

    var b = document.documentElement;
    b.setAttribute('data-useragent', navigator.userAgent);
    b.setAttribute('data-platform', navigator.platform);
    b.className += ((!!('ontouchstart' in window) || !!('onmsgesturechange' in window)) ? ' touch' : '');
});

$.fn.ignore = function (sel) {
    return this.clone().find(sel).remove().end();
};

var stopVideos = function () {
    // embedded videos
    $('iframe').each(function() {
      var src = $(this).attr('src');
      $(this).attr('src', '');
      $(this).attr('src', src);
    });
    // flash videos - jwplayer
    try {
      jwplayer('.lightbox-container').stop()
    } catch (err) {
      //
    }
}

var debounce = function (func, wait, immediate) {
    var timeout;
    return function () {
        var context = this, args = arguments;
        var later = function () {
            timeout = null;
            if (!immediate) func.apply(context, args);
        };
        var callNow = immediate && !timeout;
        clearTimeout(timeout);
        timeout = setTimeout(later, wait);
        if (callNow) func.apply(context, args);
    };
};
