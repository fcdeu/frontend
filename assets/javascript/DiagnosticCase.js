function DiagnosticCase() {
    this._exportToPdfButton = $('.btn-pdf');
    this._exportToPdfModal = $('#pdf-export');
}

DiagnosticCase.prototype.build = function () {
    if(this._exportToPdfButton.length) {
        this._exportToPdfButton.on('click', $.proxy(this._showExportToPdfModal, this));
    }
};

DiagnosticCase.prototype._showExportToPdfModal = function (e) {
    e.preventDefault();
    if(this._exportToPdfModal.length) {
        this._exportToPdfModal.show();
    }
};
