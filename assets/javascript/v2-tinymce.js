(function () {

tinymce.init({
  selector: 'textarea#tinymce',
  menubar: false,
  toolbar: "bold italic underline",
  toolbar_location: "bottom",
  statusbar: false,
});

})()
