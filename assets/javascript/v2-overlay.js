$(".navbar-item_menu").on("click", function () {
  $(".overlay.open").not(".js-navbar-item_menu-overlay").removeClass("open")
  $(".js-navbar-item_menu-overlay").toggleClass("open")
  $(".navbar-item_search").removeClass("open")
})

$(".navbar-item_profile").on("click", function () {
  $(".overlay.open").not(".js-navbar-item_profile-overlay").removeClass("open")
  $(".js-navbar-item_profile-overlay").toggleClass("open")
  $(".navbar-item_search").removeClass("open")
})

$(".navbar-item_lang").on("click", function () {
  $(".overlay.open").not(".js-navbar-item_lang-overlay").removeClass("open")
  $(".js-navbar-item_lang-overlay").toggleClass("open")
  $(".navbar-item_search").removeClass("open")
})

$(".action-button").on("click", function () {
  $(".overlay.open").removeClass("open")
  $(".js-action-button-overlay").addClass("open")
})

$(".overlay").on("click", function (e) {
  if ($(e.target).hasClass("overlay")) {
    $(this).removeClass("open")
    $(".navbar-item.active").removeClass("active")
    $(".navbar-item_search").removeClass("open")
  }
})

$(".navbar-item_search, .control-search").on("click", function () {
  $(this).toggleClass("open")
  $(".overlay.open").not(".js-navbar-item_search-overlay").removeClass("open")
  $(".js-navbar-item_search-overlay").toggleClass("open")
})

$(".control-sort").on("click", function () {
  $(this).toggleClass("open")
  $(".overlay.open").not(".js-sort-overlay").removeClass("open")
  $(".js-sort-overlay").toggleClass("open")
  $(".navbar-item_search").removeClass("open")
})

// search form

$(".search-form .form-checkbox_main").on("click", function (e) {
  e.stopPropagation()
  $(".search-form .form-checkbox_main").not(this).removeClass("open")
  $(this).toggleClass("open").removeClass("open-partially")
})

$(".sub-filter").on("click", function (e) {
  e.stopPropagation()
})

$(".search-form .filter-input_main").on("change", function (e) {
  var $this = $(this)

  if ($this.is(":checked")) {
    // close all rows
    $(".search-form .form-checkbox_main").removeClass("open")
    // row wrapper
    $wrapper = $this.parent().parent()
    // open row
    $wrapper.addClass("open")
    // open sub checkboxes if any
    $wrapper.find(".sub-checkbox .filter-input").prop("checked", true)
  }
})

$(".sub-checkbox .filter-input").on("change", function () {
  var $parent = $(this).closest(".form-checkbox_main")
  var $filters = $parent.find(".sub-checkbox .filter-input")
  var checked = 0

  $filters.each(function () {
    if ($(this).is(":checked"))
      checked++
  })

  if (checked === $filters.length)
    $parent.removeClass("open-partially")

  if (checked < $filters.length)
    $parent.addClass("open-partially")
})

$(".sub-filter .filter-select").on("change", function () {
  var $parent = $(this).closest(".form-checkbox_main")
  var $filters = $parent.find(".sub-filter .filter-select")
  var withValue = 0

  $filters.each(function () {
    if ($(this).val())
      withValue++
  })

  if (withValue === $filters.length)
    $parent.removeClass("open-partially")

  if (withValue < $filters.length)
    $parent.addClass("open-partially")
})
